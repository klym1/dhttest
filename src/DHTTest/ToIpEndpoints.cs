using System;
using System.Collections.Generic;
using System.Net;

namespace DHTTest
{
    public static class ToIpEndpoints
    {
        public static IEnumerable<IPEndPoint> Convert(Dictionary<string, int> dictionary)
        {
            foreach (var keyValue in dictionary)
            {
                var dnsEntry = IpHostEntry(keyValue);

                foreach (var ipAddress in dnsEntry)
                {
                    yield return new IPEndPoint(ipAddress, keyValue.Value);
                }
            }
        }

        private static IPAddress[] IpHostEntry(KeyValuePair<string, int> keyValue)
        {
            try
            {
                var dnsEntry = Dns.GetHostEntry(keyValue.Key);
                return dnsEntry.AddressList;
            }
            catch (Exception)
            {
                return new IPAddress[0];
            }
        }
    }
}