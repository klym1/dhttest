using System;

namespace DHTTest
{
    public static class NodeIdGenerator
    {
        public static Id GenerateId()
        {
            var bytes = new byte[20];

            Array.Copy(Guid.NewGuid().ToByteArray(), bytes, 16);
            Array.Copy(Guid.NewGuid().ToByteArray(), 0, bytes, 16, 4);

            return new Id(bytes);
        }
    }
}