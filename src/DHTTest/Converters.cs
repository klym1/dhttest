﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management.Instrumentation;
using System.Net;

namespace DHTTest
{
    public static class Converters
    {
        //[NodeCompactInfo]
        //[26  bytes      ][26  bytes][ ..... ][26  bytes]
        public static List<NodeInfo> ParseNodes(byte[] nodes)
        {
            if (nodes.Length % 26 != 0)
                throw new ArgumentException("Wrong bytes number, should be dividable by 26");

            var nodesInfp = new List<NodeInfo>();

            for (int i = 0; i < nodes.Length / 26; i++)
            {
                nodesInfp.Add(ParseCompactNodeInfo(Utils.GetSubArray(nodes, i * 26, 26)));
            }

            return nodesInfp;
        }

        //[Address ][Port   ]
        //[4 bytes ][2 bytes]
        public static IPEndPoint ParseCompactIpAddressPort(byte[] bytes)
        {
            if(bytes.Length != 6) throw new ArgumentException("Should be 6 bytes");
            
            return new IPEndPoint(
                BitConverter.ToUInt32(Utils.GetSubArray(bytes, 0, 4), 0),    //address is in network byte order (Big-Endian)
                BitConverter.ToUInt16(Utils.GetSubArray(bytes, 4, 2).Reverse().ToArray(), 0)); //port is in host order
        }

        //[Node Id ][IP     ]
        //[20 bytes][6 bytes]
        public static NodeInfo ParseCompactNodeInfo(byte[] bytes)
        {
            if (bytes.Length != 26)
                throw new ArgumentException();

            return new NodeInfo
            {
                Id = new Id(Utils.GetSubArray(bytes, 0, 20)),
                Endpoint = ParseCompactIpAddressPort(Utils.GetSubArray(bytes, 20, 6))
            };
        }

        public static byte[] ParseInfoHash(char[] rawString)
        {
            if (rawString.Length != 20) throw new ArgumentException("Should be 40 symbols");

            var result = rawString.Select(it => (byte) it).ToArray();
            return result;
        }

        public static byte[] ParseInfoHash(string rawString)
        {
            if(rawString.Length != 40) throw new ArgumentException("Should be 40 symbols");

            var result =  StringToByteArray(rawString);
            return result;
        }
        
        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
}