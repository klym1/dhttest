﻿using System;

namespace DHTTest
{
    public static class Utils
    {
        public static byte[] GetSubArray(byte[] arr, int from, int length)
        {
            var newArr = new byte[length];
            Array.Copy(arr, from, newArr, 0, length);
            return newArr;
        }
    }
}