﻿using System.Collections;
using System.Linq;
using System.Numerics;

namespace DHTTest
{
    public static class IdExt
    {

        public static BitArray Dissstance(Id one, Id two)
        {
            var bitArray1 = new BitArray(one.Raw);
            var bitArray2 = new BitArray(two.Raw);
           
            return bitArray1.Xor(bitArray2);
        }
        
        public static string GetBits(BitArray bitArray)
        {
            var bits = new bool[160];

            for (int i = 0; i < bitArray.Length; i++)
            {
                bits[i] = bitArray.Get(i);
            }

            return string.Join("", bits.Select(it => it ? "1" : "0"));
        }

        public static string GetBytes(BitArray bitArray)
        {
            var bytes = new byte[20];

            bitArray.CopyTo(bytes, 0);

            var g=  string.Join("", bytes.Select(it => $"{it:X2}"));

            return g;
        }
    }
}