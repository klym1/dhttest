﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DHTTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var rootNodes = ToIpEndpoints.Convert(new Dictionary<string, int>
            {
                ["router.bittorrent.com"] = 6881,
                ["router.utorrent.com"] = 6881,
                ["router.bitcomet.com"] = 6881,
            }).ToList();

            var client = new DHTClient();
            var myId = new Id(Converters.ParseInfoHash("2016AFDD828CF45A57AAD17A8B64D5A76E5CFFAA"));
                                            
            //magnet:?xt=urn:btih:13ef3621f73e33edcfda6bc7bcec1221526b1ebf&dn=Shrek+%282001%29+720p+BrRip+x264+-+600MB+-+YIFY&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Fzer0day.ch%3A1337&tr=udp%3A%2F%2Fopen.demonii.com%3A1337&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Fexodus.desync.com%3A6969

            var torrentFile = new Id(Converters.ParseInfoHash("6867FA1183DFA790F5C852F0617611DC0476DF8B")); 
                                                                                                           //13ef3621f73e33edcfda6bc7bcec1221526b1ebf //shrek
            Console.WriteLine("Torre info hash: " + torrentFile);
            
            //var queue = new Stack<NodeInfo>();

            Console.WriteLine("Torre info hash: " +  IdExt.GetBits(new BitArray(torrentFile.Raw)));

            var root = rootNodes[0];

            var Id = client.Ping333(myId, root).Id;

            Run(client, myId, new NodeInfo {Endpoint = root, Id = Id}, torrentFile);

            Console.WriteLine("End");
        }

        private static List<IPEndPoint> notReponsingNodes = new List<IPEndPoint>();
        private static List<IPEndPoint> visitedNodes = new List<IPEndPoint>();
        

        private static void Run(DHTClient client, Id myId, NodeInfo root, Id torrentFile)
        {
            if (notReponsingNodes.Contains(root.Endpoint))
                return;

            List<NodeInfo> closest1 = new List<NodeInfo>();
            try
            {
                Console.WriteLine($"REquest to: { IdExt.GetBytes(IdExt.Dissstance(root.Id, torrentFile))}, {root.Endpoint}");

                if(visitedNodes.Contains(root.Endpoint))
                    return;

                visitedNodes.Add(root.Endpoint);

                closest1 = client.GetPeers(myId, torrentFile, root.Endpoint);
            }
            catch (Exception ex)
            {
                notReponsingNodes.Add(root.Endpoint);
            }
            
            foreach (var nodeInfo in closest1.OrderBy(it => IdExt.Dissstance(it.Id, torrentFile), new BitComparer()))
            {
                var dist = IdExt.Dissstance(nodeInfo.Id, torrentFile);

                Console.WriteLine(IdExt.GetBytes(dist));
            }
            
            foreach (var nodeInfo in closest1.OrderBy(it => IdExt.Dissstance(it.Id, torrentFile), new BitComparer()))
            {
                Run(client, myId, nodeInfo, torrentFile);
                notReponsingNodes.Add(nodeInfo.Endpoint);
            }
        }

        public class BitComparer : IComparer<BitArray>
        {
            public int Compare(BitArray x, BitArray y)
            {
                for (int i = 0; i < x.Length; i++)
                {
                    if (x.Get(i) == y.Get(i)) continue;
                    return x.Get(i) ? 1 : -1;
                }

                return 0;
            }
        }

        public class IdComparer : IComparer<Id>
        {
            public int Compare(Id x, Id y)
            {
                for (int i = 0; i < 20; i++)
                {
                    if(x.Raw[i] == y.Raw[i]) continue;
                    return x.Raw[i] > y.Raw[i]? 1 : -1;
                }

                return 0;
            }
        }
    }
}
