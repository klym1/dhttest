﻿using System;
using System.Linq;

namespace DHTTest
{
    public struct Id
    {
        public byte[] Raw => raw ?? new byte[0];

        public byte[] raw;

        public override string ToString() => string.Join("", Raw.Select(r => $"{r:X}"));
        
        public Id(byte[] bytes)
        {
            if (bytes.Length != 20) throw new ArgumentException();
            raw = bytes ;
        }
    }
}