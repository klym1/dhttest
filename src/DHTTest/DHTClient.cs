﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using BencodeNET.Objects;
using BencodeNET.Parsing;

namespace DHTTest
{
    class DHTClient
    {
        private readonly UdpClient _udpClient = new UdpClient(1301) ;

        public DHTClient()
        {
            _udpClient.Client.ReceiveTimeout = 1000;
        }
        
        public class Response
        {
            public IPEndPoint Ip { get; set; }
            public Id Id { get; set; }
        }

        public List<NodeInfo> GetPeers(Id myId, Id torrentInfoHash, IPEndPoint rootNode)
        {
            var query = new BDictionary
            {
                {"t", "aa"},
                {"y", "q"},
                {"q", "get_peers"},
                {
                    "a", new BDictionary
                    {
                        {"id", new BString(myId.Raw)},
                        {"info_hash", new BString(torrentInfoHash.Raw)},
                    }
                }
            };

            var parser = new BencodeParser();

            var rawResult = UdpRequest(rootNode, query.EncodeAsBytes());
            
            var response = Parse(rawResult);

            var result = parser.Parse<BDictionary>(rawResult);

            var response_ = result["r"] as BDictionary;

            if (response_ == null)
            {
                return new List<NodeInfo>();
            }
            
            if (response_.ContainsKey("values"))
            {
                var list = (response_["values"] as BList)?.Value.ToArray();
                var peers = list.Select(u => Converters.ParseCompactIpAddressPort((u as BString).Value.ToArray())).ToList();

                //we got the peers!!
                File.AppendAllLines("D:\\peers.txt", peers.Select(it => it.ToString()));
            }
            
            if (response_["nodes"] == null)
            {
                return new List<NodeInfo>();
            }

            var nodes = (response_["nodes"] as BString)?.Value.ToArray() ?? new byte[0];
            
            var nodesInfp =  Converters.ParseNodes(nodes);

            return nodesInfp;
        }
        
        private Response Parse(byte[] rawResult)
        {
            var parser = new BencodeParser();
            var result = parser.Parse<BDictionary>(rawResult);
            
            var response_ = result["r"] as BDictionary;

            var id = (response_["id"] as BString)?.Value.ToArray();

            Console.WriteLine($"Node Id: {new Id(id)}");

            return new Response
            {
                Id = new Id(id)
            };
        }

        public NodeInfo Ping333(Id idBytes, IPEndPoint rootNode)
        {
            var query = new BDictionary
            {
                {"t", "aa"},
                {"y", "q"},
                {"q", "ping"},
                {"a" , new BDictionary { { "id", new BString(idBytes.Raw) } }}
            };
            
            var result2 = UdpRequest(rootNode, query.EncodeAsBytes());

            var parser = new BencodeParser();
            var result = parser.Parse<BDictionary>(result2);

            var response_ = result["r"] as BDictionary;
            var a = (response_["id"] as BString).Value;
            var id = new Id(a.ToArray());

            Console.WriteLine($"Id: {id}");

            return new NodeInfo
            {
                Id = id
            };
        }
        
        private byte[] UdpRequest(IPEndPoint rootNode, byte[] bytes)
        {
            var sendBytes = _udpClient.Send(bytes, bytes.Length, rootNode);
            if (sendBytes != bytes.Length) throw new Exception($"{sendBytes}/{bytes.Length} bytes sent");

            var endpoit = new IPEndPoint(IPAddress.Any, 0);

            return _udpClient.Receive(ref endpoit);
        }
    }

    [DebuggerDisplay("{Id} {Endpoint}")]
    public class NodeInfo
    {
        public Id Id;
        public IPEndPoint Endpoint;
    }
}
