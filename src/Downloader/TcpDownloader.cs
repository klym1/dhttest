﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DHTTest;

namespace Downloader
{
    public class TcpDownloader
    {
        private TcpClient _tcpClient = new TcpClient();

        private State _state;

        public void Start(IPEndPoint endpoint)
        {
            _tcpClient.Connect(endpoint);

            _state = new State
            {
                am_choking = true,
                peer_choking = true
            };

            var stream = _tcpClient.GetStream();

           // SendBytes(stream, MessageConverters.ToBytes(NotInterested()));

            ReceiveHandShake(stream);
            
            while (true)
            {
                if (!_state.peer_choking)
                {
                    SendBytes(stream, MessageConverters.ToBytes(RequestBlock(0, 0, 16000)));
                    SendBytes(stream, MessageConverters.ToBytes(RequestBlock(0, 0, 16000)));
                }
               
                var messages = ReceiveMessage(stream);

                ProcessMessages(messages);

                var messageTypes = messages.Select(it => it.Type).ToList();
                var gg = string.Join("/", messageTypes);

                Console.WriteLine($"[{gg}]");

                if (!_state.am_interested)
                {
                    SendBytes(stream, MessageConverters.ToBytes(Interested()));
                    _state.am_interested = true;
                }
                
                Thread.Sleep(100);
                
            }
        }

        private void ProcessMessages(List<PeerMessage> messages)
        {
            foreach (var peerMessage in messages)
            {
                if (peerMessage.Type == MessageType.Interested)
                    _state.peer_interested = true;
                if (peerMessage.Type == MessageType.NotInterested)
                    _state.peer_interested = false;
                if (peerMessage.Type == MessageType.Choke)
                    _state.peer_choking = true;
                if (peerMessage.Type == MessageType.Unchoke)
                    _state.peer_choking = false;
            }
        }

        private PeerMessage UnChocked()
        {
            return new PeerMessage
            {
                Type = MessageType.Unchoke,
                PayloadBytes = new byte[0]
            };
        }

        private PeerMessage NotInterested()
        {
            return new PeerMessage
            {
                Type = MessageType.NotInterested,
                PayloadBytes = new byte[0]
            };
        }

        private PeerMessage Chocked()
        {
            return new PeerMessage
            {
                Type = MessageType.Choke,
                PayloadBytes = new byte[0]
            };
        }

        private PeerMessage Interested()
        {
            return new PeerMessage
            {
                Type = MessageType.Interested,
                PayloadBytes = new byte[0]
            };
        }

        private PeerMessage RequestBlock(int index, int begin, int length)
        {
            var indexBytes = IntToBytes(index);
            var beginBytes = IntToBytes(begin);
            var lengthBytes = IntToBytes(length);

            return new PeerMessage
            {
                Type = MessageType.Request,
                PayloadBytes = new[]
                {
                    indexBytes[3],
                    indexBytes[2],
                    indexBytes[1],
                    indexBytes[0],

                    beginBytes[3],
                    beginBytes[2],
                    beginBytes[1],
                    beginBytes[0],

                    lengthBytes[3],
                    lengthBytes[2],
                    lengthBytes[1],
                    lengthBytes[0],
                }
            };
        }

        private byte[] IntToBytes(int val)
        {
            return BitConverter.GetBytes(val);
        }

        private List<PeerMessage> ReceiveMessage(NetworkStream stream)
        {
            Console.WriteLine("Received");

           var memoryStream = new MemoryStream();

            var buffer = new byte[1024];

            while (stream.DataAvailable)
            {
                var readBytes = stream.Read(buffer, 0, 1024);
                memoryStream.Write(buffer, 0, readBytes);
            }

            var receivedBytes = memoryStream.ToArray();

            DumpBytes(receivedBytes);

            if (receivedBytes.Length == 0)
                Console.WriteLine("empty response");

            if (receivedBytes.Length != 0)
                return ParseMessages(receivedBytes);
            
            return new List<PeerMessage>(0);
        }

        private List<PeerMessage> ParseMessages(byte[] receivedBytes)
        {
            var stream = new MemoryStream(receivedBytes);
            var binaryReader = new BinaryReader(stream);

            var list  = new List<PeerMessage>();

            while (binaryReader.BaseStream.CanRead)
            {
                var lengthBytes = binaryReader.ReadBytes(4);
                if(lengthBytes.Length == 0)
                    break;
                
                var length = ToInt(lengthBytes);

                if (length == 0)
                {
                    Console.WriteLine("Keep alive from host");
                    continue;
                }

                var payload = binaryReader.ReadBytes(length);

                list.Add(new PeerMessage
                {
                    Type = (MessageType)payload[0],
                    PayloadBytes = payload,
                });
            }

            return list;
        }

        private int ToInt(byte[] readBytes)
        {
            return BitConverter.ToInt32(
                new[]
                {
                    readBytes[3],
                    readBytes[2],
                    readBytes[1],
                    readBytes[0],
                }, 0);
        }

        private void ReceiveHandShake(NetworkStream stream)
        {
            while (true)
            {
                var handShakeMessage = GetHandShake();

                SendBytes(stream, handShakeMessage);

                var handshake = ToHandshake(stream);

                DumpBytes(new[] { handshake.Length });
                DumpBytes(handshake.Protocol);
                DumpBytes(handshake.Empty);
                DumpBytes(handshake.InfoHash);
                DumpBytes(handshake.PeerId);

                // DumpBytes(buffer.Skip(position).ToArray());
                return;
            }

        }

        private byte[] GetHandShake()
        {
            var infoHash = Converters.ParseInfoHash("1FC0B473B0AB37580EC1FEC31637985CDB4F7610");
            var peerId = new Id(Converters.ParseInfoHash("-ES2060-155206061234".ToArray())).Raw;

            return new byte[][]
            {
                new byte[] { 19 },
                BitTorrentProtocol(),
                ReservedBytes,
                infoHash,
                peerId
            }.SelectMany(it => it).ToArray();

         //   SendBytes(stream, );
        //    SendBytes(stream, BitTorrentProtocol());
          //  SendBytes(stream, ReservedBytes);
          //  SendBytes(stream, infoHash);
         //   SendBytes(stream, peerId);
        }

        public byte[] ReservedBytes => new byte[] {0, 1, 2, 3, 4, 5, 6, 7};

        public void SendBytes(NetworkStream networkStream, byte[] bytes)
        {
            networkStream.Write(bytes, 0, bytes.Length);
            Console.WriteLine("Send bytes");
            DumpBytes(bytes);
        }

        private void DumpBytes(byte[] bytes, int? length = null) => Console.WriteLine(Tostr(bytes, length));

        private string Tostr(byte[] bytes, int? legnth = null)
            => string.Join("", bytes.Take(legnth ?? int.MaxValue).Select(it => $"{it:X2}"));

        private byte[] BitTorrentProtocol()
        {
            return EncodeString("BitTorrent protocol");
        }

        private static Handshake ToHandshake(NetworkStream stream)
        {
            //while (!stream.DataAvailable)
            {
                //Thread.Sleep(1000);
            }
            
            using (var binaryReader = new BinaryReader(stream, Encoding.ASCII, true))
            {
                var Length = binaryReader.ReadByte();
                var Protocol = binaryReader.ReadBytes(19);
                var Empty = binaryReader.ReadBytes(8);
                var InfoHash = binaryReader.ReadBytes(20);
                var PeerId = binaryReader.ReadBytes(20);

                var a = new Handshake
                {
                   Length = Length,
                   Protocol = Protocol,
                   Empty = Empty,
                   InfoHash = InfoHash,
                   PeerId = PeerId
                };
                
                return a;
            }
        }

        private byte[] EncodeString(string val) => Encoding.ASCII.GetBytes(val);
    }



    public struct Handshake
    {
        public byte Length;
        public  byte[] Protocol;
        public  byte[] Empty;
        public  byte[] InfoHash;
        public byte[] PeerId;
    }

    [DebuggerDisplay("{Type} {PayloadBytes.Length} bytes")]
    public class PeerMessage
    {
        public MessageType Type;    
        public byte[] PayloadBytes;
    }

    public class State
    {
        public bool am_choking;
        public bool am_interested;
        public bool peer_choking;
        public bool peer_interested;
    }

    public enum MessageType
    {
        Choke,
        Unchoke,
        Interested,
        NotInterested,
        Have,
        Bitfield,
        Request,
        Piece,
        Cancel =8,
        Port = 9
    }
}