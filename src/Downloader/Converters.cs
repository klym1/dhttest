﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Downloader
{
    class MessageConverters
    {
        public static byte[] ToBytes(PeerMessage mes)
        {
            var payloadSize = mes.PayloadBytes.Length;
            var result = new byte[payloadSize + 1 + 4];

            var bitconverter = BitConverter.GetBytes(payloadSize + 1);

            result[0] = bitconverter[3];
            result[1] = bitconverter[2];
            result[2] = bitconverter[1];
            result[3] = bitconverter[0];

            result[4] = (byte)mes.Type;

            Array.Copy(mes.PayloadBytes, 0, result, 5, payloadSize);
            return result;
        }

        public PeerMessage ToMessage(byte[] mes)
        {
            using (var binaryReader = new BinaryReader(new MemoryStream(mes)))
            {
                var length = binaryReader.ReadInt32();
                var type = (MessageType) binaryReader.ReadByte();
                var payload = length > 0 ? binaryReader.ReadBytes(length - 1) : new byte[0];

                return new PeerMessage
                {
                    Type = type,
                    PayloadBytes = payload
                };
            }
        }
    }
}
